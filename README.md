# Ansible Initial Server Setup on Ubuntu 20.04

## Automatisation avec Ansible
Ce repo contient les rôles Ansible suivants :
- ```setup-server```
    - Installer des paquets spécifiques
    - Créer le groupe wheel (sudo)
    - *TODO - Paramétrer ufw pour exposer seulement le port 22 (SSH)*
- ```create-user```
    - Créer le home du user
    - Lui assigner les droits sudo ou non
    - Copier sa clef SSH
- ```docker```
    - Submodule permettant d'installer Docker et Docker-compose [Source](https://github.com/geerlingguy/docker-examples)
- ```dotfiles```
<<<<<<< HEAD
    - Télécharger et set up les simples dotfiles du user mofo
- ```docker```
    - Submodule Git pour installer docker et docker compose

## CI/CD avec Gitlab et Molecule
La CI/CD est réalisée avec Gitlab-CI et s'appuie sur une image ```docker:dind```, nécessaire pour Molecule. En effet, dans notre pipeline, Molecule lance un conteneur pour déployer et tester le rôle Ansible et a donc besoin de pouvoir lancer des commandes Docker. Dans le cas de notre pipeline, nous indiquons également à Molecule d'utiliser les frameworks Python de tests : pytest et testinfra.

Les rôles Ansibles testés dans la pipeline sont : 
- le rôle ```create-user``` avec le simple test suivant :
    - les users mofo et guigui sont créés
    - mofo est sudo et guigui ne l'est pas
    - tester par whitelist (c'est à dire qu'il n'y a pas d'autres users que mofo et guigui qui soient créés)
*TODO*
- le rôle ```setup-server``` avec les simples tests suivants :
    - le groupe wheel existe
    - des paquets spécifiques sont installés (curl, git, vim...)

## Utilisation du dépôt (pour un nouvel arrivant)
### Spécifier le(s) host(s) sur qui tester les playbooks dans le fichier ```hosts.yml```

*Exemple de host :*
```
all:
  hosts:
    server1:
      ansible_user: root
      ansible_host: test1.mofo.ovh (ou adresse IP)
```

### Tester les playbooks
```
ansible-playbook playbooks/main.yml -i hosts.yml
```
*Tester un playbook en particulier en utilisant les tags (ici setup-server)*
```
ansible-playbook playbooks/main.yml -i hosts.yml --tags setup-server
```

### Ajouter un rôle
Les rôles du présent dépôt sont organisés comme suivant :
```
tree roles/ -L 1

roles/
├── create-user
├── dotfiles
├── docker
└── setup-server
```

Il est possible d'ajouter un rôle de plusieurs manières différentes :
- En créant le rôle à la main et en utilisant l'arborescence du dossir de notre choix comme par exemple dans le cas du role setup-server :
```
tree roles/setup-server/ -L 1

roles/setup-server/
├── defaults
├── molecule
└── tasks
```

- En se servant de Molecule pour créer un rôle avec une arborescence toute faite (ici role-test):
```
molecule init role role-test

--> Initializing new role role-test...
Initialized role in /tmp/testlkj/role-test successfully.

tree role-test/

role-test/
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── molecule
│   └── default
│       ├── converge.yml
│       ├── INSTALL.rst
│       ├── molecule.yml
│       └── verify.yml
├── README.md
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml
```
- En clonant un rôle existant avec les git submodule (ici avec le role ansible-role-podman du user Github chasinglogic)
```
git submodule add git@github.com:chasinglogic/ansible-role-podman.git roles/podman
```

### Ajouter des tests infras pour Molecule
Les tests lancés sur les playbooks s'appuient donc sur les frameworks Python testinfra et pytest. Pour ajouter un test sur un role, il faut modifier le fichier se trouvant dans le dossier test, au sein du dossier molecule.

*Exemple d'un fichier de tests*
```
cat -p roles/setup-server/molecule/default/tests/test_defaults.py

import os
import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
  os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_group_wheel_exists(host):
  wheel_group = host.group("wheel")
  assert wheel_group.exists


@pytest.mark.parametrize("name", [
    ("curl"),
    ("vim"),
    ("git"),
    ("ufw"),
    ("net-tools"),
])
def test_packages(host, name):
    pkg = host.package(name)
    assert pkg.is_installed
```

### Déclarer les tests Molecule dans la pipeline
Pour définir les roles à tester dans notre pipeline, il faut les déclarer dans le fichier ```.gitlab-ci.yml``` comme suivant (et ne pas oublier d'installer les Frameworks de test dans la partie before_script) : 
```
image: docker:stable-dind

services:
  - docker:dind

before_script:
  - apk add --no-cache
    python3 python3-dev py3-pip gcc git curl build-base
    autoconf automake py3-cryptography linux-headers
    musl-dev libffi-dev openssl-dev openssh
  - docker info
  - python3 --version
  - python3 -m pip install ansible molecule[docker]===3.0.6 pytest testinfra
  - ansible --version
  - molecule --version

molecule:
  stage: test
  script:
    - cd roles/setup-server && molecule test
    - cd ../create-user && molecule test
```
Par exemple, si l'on voulait ajouter un test nommé **test-random**, il faut dire au shell qui lance les étapes de tests de se déplacer dans l'arborescence de notre projet, pour y lancer la commande Molecule. 

Il faudrait pour cela ajouter la ligne suivante à la fin de la partie script du stage molecule :
```
- cd ../../roles/test-random && molecule test
```

## Difficultés rencontrées
- Perte de temps en utilisant des images pour Molecule qui ne possédaient pas python3 --> fixed en utilisant les images de geerlinger
- Confusion avec l'arborescence de Ansible et Molecule
- L'instance publique de Gitlab ne semble pas permettre de tester les ouvertures de ports ou d'installer Podman par exemple
- Structure et bonnes pratiques du fichier gitlab-ci.yml (à creuser/améliorer)

## Useful Resources
- [Jeff Gerling: Testing your Ansible roles with Molecule](https://www.jeffgeerling.com/blog/2018/testing-your-ansible-roles-molecule)
- [Using Molecule to test Ansible roles](https://digitalis.io/blog/using-molecule-to-test-ansible-roles/)
- [Test infra examples](https://github.com/philpep/testinfra/blob/master/test/test_modules.py)
- [Comment tester des rôles Ansible avec Molecule sur Ubuntu 18.04](https://www.codeflow.site/fr/article/how-to-test-ansible-roles-with-molecule-on-ubuntu-18-04)
- [Using molecule with GitLab CI](https://univers-libre.net/posts/ansible-molecule-gitlab-ci.html)
- [Best practices for building docker images with GitLab CI](https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/)
- [Introduction à Gitlab CI/CD](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/)
- [How To Test Ansible Roles with Molecule on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-test-ansible-roles-with-molecule-on-ubuntu-18-04)
- [Tests infras](https://austincloud.guru/2018/09/18/adding-a-molecule-to-an-existing-ansible-role/)
- [Use MySQL with the Docker executor](https://docs.gitlab.com/ee/ci/services/mysql.html#use-mysql-with-the-docker-executor)
- [Gitlab artifacts](https://docs.gitlab.com/ee/ci/yaml/#artifacts)
