import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
  os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

@pytest.mark.parametrize("user", [
    ("mofo"),
    ("guigui"),
])
def test_user_exists(host, user):
  host_user = host.user(user)
  assert host_user.exists

# TODO is sudo