import os
import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
  os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_group_wheel_exists(host):
  wheel_group = host.group("wheel")
  assert wheel_group.exists


@pytest.mark.parametrize("name", [
    ("curl"),
    ("vim"),
    ("git"),
    ("ufw"),
    ("net-tools"),
])
def test_packages(host, name):
    pkg = host.package(name)
    assert pkg.is_installed


# TODO podman test
# def test_run_hello_world_container_successfully(host):
#     hello_world_ran = host.run("docker run hello-world")
#     assert 'Hello from Docker!' in hello_world_ran.stdout 